## iQey Import API V1
Documentation

__Docs version:__ 1.1.0




# Request structure
When sending a request to the API, there are some parameters that is required to be sent in the header.

| Type     | URL							|
|:--------:|:------------------------------:|
| GET/POST | https://api.iqey.org/v1/__:endpoint__ |

__:endpoint__ is an URL-parameter that will lead your request to the API's correct endpoint.


You will need to include a PIN-code in every request to the API.
Please add the field in the __header__ of the request.
If you have forgotten, or have never received your pin, please contact us.

| Param.   		| Type		| Description							|
|:-------------:|:---------:|:-------------------------------------:|
| pin			| integer	| A PIN-code representing your company.|




# Response structure
When sending a successfull request to our API, you will always get a response.

```javascript
{
    success: true,
    msg: "Successfully found the user-ID.",
    someString: "abc123"

}
```

| Param.   		| Type		| Description							|
|:-------------:|:---------:|:-------------------------------------:|
| success		| boolean	| Returns false if any error occurs during function. If not, it will return true.|
| msg			| string	| A message explaining the successful or un un-successful function.|
| someString	| string	| Just to illustrate. A endpoint can return data on different strings.|

If the success return false, the msg will return a simplified explanation of the error that occurred during the request.
Different endpoint will give you different string with data in return, but every endpoint will include success and msg.




# Testing the API
You can test your PIN and the API by sending a simple request to our test-endpoint.

| Type     | URL							|
|:--------:|:------------------------------:|
| POST | https://api.iqey.org/v1/test |


You will get a response like this.

```javascript
{
    success: true, // can also be false
    msg: "Your PIN is working!",
}
```

If you the success returns false, then there may be something wrong with your request.
Please check that you have included your pin like shown above, and try again.
Still getting false? Please contact us, and we will assist you and check for errors.




# Create center and center-chain
This endpoint will be used to create center and center-chains.

> If a fitness center is not part of any center-chain, you don't need to create a chain.

If you want to start a new chain, please execute following request.

| Type     | URL							      |
|:--------:|:------------------------------------:|
| POST | https://api.iqey.org/v1/center/chain |

With following parameters.

| Req. | Param.   		| Type		| Description							|
|:-:|:-------------:|:---------:|:-------------------------------------:|
| * | name          | string    | Name of the chain.|

If you want to create a new center, and/or want to add it to a chain, please execute following request.

| Type     | URL							      |
|:--------:|:------------------------------------:|
| POST | https://api.iqey.org/v1/center/sub |

With following parameters.

| Req. | Param.   		| Type		| Description							|
|:-:|:-------------:|:---------:|:-------------------------------------:|
| * | name          | string    | Name of the center.|
| * | phone         | string    | Center's phone-number.|
|   | website       | string    | Center's website-address.|
| * | locX		    | string    | Center's X-coordinate.|
| * | locY		    | string    | Center's Y-coordinate.|
|   | chainID		| int       | Chain's ID.|

The request will give you a response including your new chainID/centerID.

```javascript
{
    success: true, // can also be false
    msg: "Successfully created a new center.",
    centerID: 123456789ABCD, // only on sub-request
    chainID: 123456789ABCD // only on chain-request
    }
}

```

> The API does not provide any endpoint for bulk-importing fitness-centers or center-chains.




# Bulk import users
This endpoint will be used to import multiple users to the iQey-database.
In the bulk import function you have to send a file and some parameters.

You will have to do one import for each unique fitness center, not one big file for a whole chain of fitness centers.

> A person can be member of multiple fitness centers withouth any problems. Our API will merge similar accounts.

| Type     | URL							|
|:--------:|:------------------------------:|
| POST | https://api.iqey.org/v1/bulk |


The request should include the following parameters.

| Param.   		| Type		| Description							|
|:-------------:|:---------:|:-------------------------------------:|
| file			| file	    | A .csv file containing all the users.|
| centerID		| int	    | ID of the center the users will be assigned to.|


The file should be a .CSV file, comma-separated.

> __NOTE:__ We do NOT recommend using Microsoft's Excel for exporting the CSV-file due to errors with special characters like �, � and �.
> Please use something like Google Spreadsheet or LibreOffice. Remember to export with UTF-8.

> We recommend to download our finished CSV-template. [Download here].
[Download here]:https://bitbucket.org/waysas/iqey-import-api/downloads/

Please include following headers in the file:

| Req. | Param.   		| Type		| Description							|
|:-:|:-------------:|:---------:|:-------------------------------------:|
| * | customerID    | string    | Center-provided customer-ID.|
| * | firstName		| string    | First name.|
| * | lastName		| string    | Last name.|
| * | birth		    | string    | Birth date. _Format: dd/mm/yyyy_|
| * | email        	| string    | Email.|
|   | phone		    | string    | Phone.|
|   | phoneMobile	| string    | Phone.|
|   | phoneWork		| string    | Phone.|
| * | gender	    | int       | Gender _Format: 1 = man, 2 = women, 3 = not specified_|
| * | cardNumber	| string    | Number of the user's card.|
|   | membership	| string    | Membership description. _Example: VIP_|
|   | startDate    	| string    | Membership start date. _Format: dd/mm/yyyy_|
|   | endDate    	| string    | Membership end date. _Format: dd/mm/yyyy_|
|   | status    	| boolean   | Membership status. _Default: true_|


After the file has been uploaded, and our algorithm has run through the file, you will receive a detailed response.
If the algorithm finds any error in a user-row it will skip it and go to the next one.

```javascript
{
    success: true, // can also be false
    msg: "The process is finished.",
    batchID: abc123,
    failed: [
        {
            line: 3,
            msg: "[error message]"
        }
    ]
}
```

The success does not return false if the endpoint fail to import some users.
It only returns false if there is a problem with the whole file, or if something required is missing (like parameters or columns).
The reason for the false will be explained by msg.

The response will only contain failed array if any users failed to import.
If this happens, you __must create a new file__. Please _don�t_ use the same file with changes. 
Importing the same file without removing the sucessfully imported users will result in double-imports.

> TIP: Every response from import includes a _batchID_. This is assigned so that we can undo an import.
> If you have done a mistake in your import, please contact us so that we can remove the old import.




# Single import user
This endpoint will be used to import a single user to the iQey-database.

> A person can be member of multiple fitness centers withouth any problems. Our API will merge similar accounts.

| Type     | URL							|
|:--------:|:------------------------------:|
| POST | https://api.iqey.org/v1/single |


The following parameters can be used.

| Req. | Param.   		| Type		| Description							|
|:-:|:-------------:|:---------:|:-------------------------------------:|
| * | customerID    | string    | Center-provided customer-ID.|
| * | firstName		| string    | First name.|
| * | lastName		| string    | Last name.|
| * | birth		    | string    | Birth date. _Format: dd/mm/yyyy_|
| * | email        	| string    | Email.|
|   | phone		    | string    | Phone.|
|   | phoneMobile	| string    | Phone.|
|   | phoneWork		| string    | Phone.|
| * | gender	    | int       | Gender _Format: 1 = man, 2 = women, 3 = not specified_|
| * | cardNumber	| string    | Number of the user's card.|
|   | membership	| string    | Membership description. _Example: VIP_|
|   | startDate    	| string    | Membership start date. _Format: dd/mm/yyyy_|
|   | endDate    	| string    | Membership end date. _Format: dd/mm/yyyy_|
| * | centerID    	| string    | Center the user should be assigned to.|
|   | status    	| boolean   | Membership status. _Default: true_|

After the request is sent, you will get a response.

```javascript
{
  success: true,
  msg: "Successfully imported a new single user with name _____!"
}
```

### Questions?
Please contact our backend-developer at e-mail: ola@ways.as